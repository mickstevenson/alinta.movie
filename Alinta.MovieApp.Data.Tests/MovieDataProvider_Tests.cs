﻿using System.Linq;
using Alinta.MovieApp.Data;
using Alinta.MovieApp.Data.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Alita.Movie.Data.Tests
{
    [TestClass]
    public class MovieDataProvider_Tests
    {
        [TestMethod]
        public void MovieDataProvider_MovieContainsDuplicateRoles_SingleRoleReturned()
        {
            var repo = new Mock<IMovieRepository>();

            var movies = new[]
            {
                new Alinta.MovieApp.Data.Model.Movie
                {
                    Name = "Family Guy",
                    Roles = new[]
                    {
                        new Role {Actor = "Mila Kunis", Charcter = "Meg Griffin"},
                        new Role {Actor = "Mila Kunis", Charcter = "Meg Griffin"},
                    }
                },
                new Alinta.MovieApp.Data.Model.Movie
                {
                    Name = "Family Guy",
                    Roles = new[]
                    {
                        new Role {Actor = "Mila Kunis", Charcter = "Meg Griffin"},
                        new Role {Actor = "Mila Kunis", Charcter = "Meg Griffin"},
                        new Role {Actor = "Mila Kunis", Charcter = "Meg Griffin"},
                    }
                },
            };

            repo.Setup(m => m.GetMovies()).ReturnsAsync(movies);

            var dataProvider = new MovieDataProvider(repo.Object);
            var task = dataProvider.GetActors();
            task.Wait();

            var result = task.Result;

            Assert.IsTrue(result.Count() == 1);
            Assert.IsTrue(result.First().Roles.Count() == 1);
            Assert.IsTrue(result.First().Name == "Mila Kunis");
            Assert.IsTrue(result.First().Roles.First().Movie == "Family Guy");
            Assert.IsTrue(result.First().Roles.First().Character == "Meg Griffin");
        }

        [TestMethod]
        public void MovieDataProvider_GetActors_SortsRolesByMovieName()
        {
            var repo = new Mock<IMovieRepository>();

            var movies = new[]
            {
                new Alinta.MovieApp.Data.Model.Movie
                {
                    Name = "The Blues Brothers",
                    Roles = new[]
                    {
                        new Role {Actor = "John Belushi", Charcter = "Jake Blues"},
                    }
                },
                new Alinta.MovieApp.Data.Model.Movie
                {
                    Name = "Animal House",
                    Roles = new[]
                    {
                        new Role {Actor = "John Belushi", Charcter = "John Blutarsky"},
                    }
                },
            };

            repo.Setup(m => m.GetMovies()).ReturnsAsync(movies);

            var dataProvider = new MovieDataProvider(repo.Object);
            var task = dataProvider.GetActors();
            task.Wait();

            var result = task.Result;

            Assert.IsTrue(result.Count() == 1);
            Assert.IsTrue(result.First().Roles.Count() == 2);
            Assert.IsTrue(result.First().Name == "John Belushi");
            Assert.IsTrue(result.First().Roles.First().Character == "John Blutarsky");
        }
    }
}
