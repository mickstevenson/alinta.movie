﻿using System;
using System.Configuration;
using System.Threading.Tasks;
using System.Web.Mvc;
using Alinta.MovieApp.Data;
using Alinta.MovieApp.Web.Models;

namespace Alinta.MovieApp.Web.Controllers
{
    public class ActorsController : Controller
    {
        public async Task<ActionResult> List()
        {
            var baseUrl = new Uri(ConfigurationManager.AppSettings["ApiUrl"]);
            var repo = new MovieRepository(baseUrl);
            var dataProvider = new MovieDataProvider(repo);
            var actors = await dataProvider.GetActors();

            var actorsViewModel = new ActorsViewModel
            {
                Actors = actors,
            };

            return View(actorsViewModel);
        }
    }
}