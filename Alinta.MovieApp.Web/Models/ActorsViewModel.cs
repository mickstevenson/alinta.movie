using System.Collections.Generic;
using Alinta.MovieApp.Data.Model;

namespace Alinta.MovieApp.Web.Models
{
    public class ActorsViewModel
    {
        public IEnumerable<Actor> Actors { get; set; }
    }
}