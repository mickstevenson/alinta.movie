﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Alinta.MovieApp.Data.Model;

namespace Alinta.MovieApp.Data
{
    public interface IMovieDataProvider
    {
        Task<IEnumerable<Actor>> GetActors();
    }
}