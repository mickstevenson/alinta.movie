﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Alinta.MovieApp.Data.Model;

namespace Alinta.MovieApp.Data
{
    public interface IMovieRepository
    {
        Task<IEnumerable<Movie>> GetMovies();
    }
}