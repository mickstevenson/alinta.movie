﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Alinta.MovieApp.Data.Model;

namespace Alinta.MovieApp.Data
{
    public class MovieRepository : IMovieRepository
    {
        private readonly Uri _baseApiUrl;

        public MovieRepository(Uri baseBaseApiUrl)
        {
            _baseApiUrl = baseBaseApiUrl;
        }

        public async Task<IEnumerable<Movie>> GetMovies()
        {
            IEnumerable<Movie> movies = null;

            var client = GetClient();
            var response = await client.GetAsync(@"api//movies");

            if (response.IsSuccessStatusCode)
                movies = await response.Content.ReadAsAsync<IEnumerable<Movie>>();

            return movies;
        }

        private HttpClient GetClient()
        {
            var client = new HttpClient
            {
                BaseAddress = _baseApiUrl,
            };

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return client;
        }
    }
}