﻿namespace Alinta.MovieApp.Data.Model
{
    public class MovieRole
    {
        public string Character { get; set; }
        public string Movie { get; set; }
    }
}