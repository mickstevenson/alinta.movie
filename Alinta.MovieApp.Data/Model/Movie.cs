﻿using System.Collections.Generic;

namespace Alinta.MovieApp.Data.Model
{
    public class Movie
    {
        public string Name { get; set; }
        public IEnumerable<Role> Roles { get; set; }
    }
}