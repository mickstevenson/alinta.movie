using Newtonsoft.Json;

namespace Alinta.MovieApp.Data.Model
{
    public class Role
    {
        [JsonProperty("name")]
        public string Charcter { get; set; }
        public string Actor { get; set; }
    }
}