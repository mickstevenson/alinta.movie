﻿using System.Collections.Generic;

namespace Alinta.MovieApp.Data.Model
{
    public class Actor
    {
        public string Name { get; set; }
        public IEnumerable<MovieRole> Roles { get; set; }
    }
}