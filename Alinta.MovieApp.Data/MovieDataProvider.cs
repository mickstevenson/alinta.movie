﻿using System.Collections.Generic;
using Alinta.MovieApp.Data.Model;
using System.Linq;
using System.Threading.Tasks;

namespace Alinta.MovieApp.Data
{
    public class MovieDataProvider : IMovieDataProvider
    {
        public MovieDataProvider(IMovieRepository repo)
        {
            _repo = repo;
        }

        public async Task<IEnumerable<Actor>> GetActors()
        {
            var movies = await _repo.GetMovies();

            if (movies == null || !movies.Any())
                return Enumerable.Empty<Actor>();

            var movieRoles = movies
                .SelectMany(movie => movie.Roles.Select(role => new {MovieName = movie.Name, Role = role}))
                .GroupBy(movieRole => new {movieRole.MovieName, movieRole.Role.Charcter, movieRole.Role.Actor})
                .Select(grp => grp.First());

            var actors = movieRoles
                .GroupBy(movieRole => movieRole.Role.Actor ?? string.Empty)
                .Select(grp => new Actor
                {
                    Name = string.IsNullOrEmpty(grp.Key) ? "unknown actor" : grp.Key,
                    Roles = grp.Select(movieRole =>
                        new MovieRole
                        {
                            Character = string.IsNullOrEmpty(movieRole.Role.Charcter)
                                ? "unknown character"
                                : movieRole.Role.Charcter,
                            Movie = string.IsNullOrEmpty(movieRole.MovieName)
                                ? "unknown movie name"
                                : movieRole.MovieName,
                        }).OrderBy(mr => mr.Movie)
                }).OrderBy(actor => actor.Name);

            return actors;
        }

        private readonly IMovieRepository _repo;
    }
}